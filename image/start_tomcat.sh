#!/bin/bash

SERVER_CONF_FILE="/etc/tomcat8/server.xml"
KEYSTORE_FILE="/etc/tomcat8/server.jks"
DEFAULT_PASS="password"

DB_HOST="localhost"
DB_PORT="5432"
SLEEP="10"

WAIT=0
HELP=0
RUN_TEST=0

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --wait-for-db)
    PSQL_HOST=$2
    WAIT=1
    shift
    ;;
    --sleep)
    SLEEP=$2
    shift
    ;;
    --keystore-pass)
    PASSWORD=$2
    shift
    ;;
    --test)
    RUN_TEST=1
    ;;
    -h|--help)
    HELP=1
    ;;
    *)
    echo "Unkown option: $key"
    MODE_HELP=1
    ;;
esac
shift # past argument or value
done

if [ "${HELP}" -eq 1 ]; then
    echo ""
    echo "start_tomcat.sh [args]"
    echo ""
    echo "Where [args] is one of:"
    echo "  --wait-for-db hostname     Wait for the database running on [hostname] to start"
    echo "  --sleep seconds            Check every [seconds] seconds"
    echo "  --keystore-pass password   User [password] to protect the keystore"
    echo ""
    echo "  -h, --help       Show help"
    echo ""
    exit 1
fi

if [ "${RUN_TEST}" -eq 1 ]; then
    echo "Forking check_test.sh"
    check_test.sh "tomcat8" &
fi

#
# Check if a user defined store and key pass are supplied
#
if [ -z "${PASSWORD}" ]; then
	echo "Using default STORE_PASS: ${DEFAULT_PASS}"
	PASSWORD="${DEFAULT_PASS}"
else
	echo "Using supplied PASSWORD"
fi

#
# Generate SSL certificate if the keystore does not exist
#
if [ -f "${KEYSTORE_FILE}" ]; then
	echo "Using existing ssl certificate from ${KEYSTORE_FILE}"
else
	echo "Generating new ssl certificate in ${KEYSTORE_FILE}"
	keytool -genkey \
		-keyalg RSA \
		-alias selfsigned \
		-keystore "${KEYSTORE_FILE}" \
 		-storepass "${PASSWORD}" \
		-validity 360 \
		-keysize 2048 \
		-noprompt \
		-dname "cn=clarin.eu, ou=CLARIN, o=CLARIN-ERIC, c=NL" \
		-keypass "${PASSWORD}"
	
	#Exit if keytool command failed
	rc=$?;
	if [[ $rc != 0 ]]; then
		echo "Failed to generate ssl certificate";
		exit $rc;
	fi

	#update server.xml with correct keystore pass
	if [ "${PASSWORD}" != "password" ]; then
		echo "Changing keystorepass in ${SERVER_CONF_FILE}" 
		sed -i "s/password/${PASSWORD}/g" "${SERVER_CONF_FILE}"
	fi
fi

#
# Start tomcat in foreground
#
if [ "${WAIT}" -eq 1 ]; then
    while ! echo exit | nc "${PSQL_HOST}" "${DB_PORT}" </dev/null; do
        echo 'Waiting for database to become available'; sleep "${SLEEP}";
    done
fi

echo "Starting tomcat"
/usr/share/tomcat8/bin/catalina.sh run

#Ensure we exit with a clean code when in test mode
EXIT=$?
if [ "${RUN_TEST}" -eq 1 ]; then
    if [ "${EXIT}" -eq 143 ]; then
        exit 0
    fi
fi

exit "${EXIT}"
